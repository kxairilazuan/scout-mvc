<%@ include file="sidebar/sidebar_header.jspf" %>
	<div class="content">
		<%@ include file="top_nav.jspf" %>
		<div class="p-2">
			<div class="card w-100">
				<h4 class="card-header">Badges</h4>
				<ul class="list-group list-group-flush">
					<li class="list-group-item p-1 overflow-auto">
						<div class="accordion" id="accordionPanelsStayOpenExample">
							<div id="accordian-zero" class="d-none accordion-item">
						        <h2 class="accordion-header">
						            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseZero" aria-expanded="true" aria-controls="panelsStayOpen-collapseZero">
						            KING SCOUT BADGE
						            </button>
						        </h2>
						        <div id="panelsStayOpen-collapseZero" class="accordion-collapse collapse show">
						            <div class="accordion-body">
						            	<div class="row g-2">
						            		<div class="col-lg-3 col-12 my-1">
						            			<div class="card">
						            				<img height="250" class="card-img-top" src="/image/Raja.png">
						            				<div class="card-body border-top">
						            					<h5 class="card-title">KING SCOUT BADGE</h5>
						            					<p class="card-text">FORM 5</p>
						            				</div>
						            			</div>
						            		</div>
						            	</div>
						            </div>
						        </div>
						    </div>
						    <div class="accordion-item">
						        <h2 class="accordion-header">
						            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
						            SENIOR BADGE
						            </button>
						        </h2>
						        <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show">
						            <div class="accordion-body">
						            	<div id="accordion-bodyOne" class="row g-2"></div>
						            </div>
						        </div>
						    </div>
						    <div class="accordion-item">
						        <h2 class="accordion-header">
						            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="true" aria-controls="panelsStayOpen-collapseTwo">
						            JUNIOR BADGE
						            </button>
						        </h2>
						        <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse show">
						            <div class="accordion-body">
						            	<div id="accordion-bodyTwo" class="row g-2"></div>
						            </div>
						        </div>
						    </div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		function onWindowLoad() {
			onGetbadges();
			onGetAssessmentApplication();
		}
		
		function onGetbadges() {
			$.ajax({
				type: "GET",
		        url: "/core/studentBadges/student/all/" + sessionStorage.getItem("login_id")
		    }).then(function(dataList) {
		    	for (let data of dataList) {
			    	if (data.studentBadgeId && data.studentBadgeStatus === 'APPROVE') {
			    		if (data.badge.badgeCategory === 'SENIOR BADGE') {
			    			$('#accordion-bodyOne').append(
			    				$('<div>').addClass('col-lg-3').addClass('col-12').addClass('my-1').append(
		    						$('<div>').addClass('card')
		    						.append(
		    							$('<img>').height(250).addClass('card-img-top').attr('src', '/core/pictures/' + data.badge.badgeImage)
		    						)
		    						.append(
	    								$('<div>').addClass('card-body').addClass('border-top').append(
	   										$('<h5>').addClass('card-title').html(data.badge.badgeName)
	    			    				).append(
	   										$('<p>').addClass('card-text').html('FORM ' + data.badge.badgeForm)
	    			    				)
				    				)
			    				)
			    			);
			    		} else if (data.badge.badgeCategory === 'JUNIOR BADGE') {
			    			$('#accordion-bodyTwo').append(
			    				$('<div>').addClass('col-lg-3').addClass('col-12').addClass('my-1').append(
		    						$('<div>').addClass('card')
	    							.append(
		    							$('<img>').height(250).addClass('card-img-top').attr('src', '/core/pictures/' + data.badge.badgeImage)
		    						)
		    						.append(
	    								$('<div>').addClass('card-body').addClass('border-top').append(
	   										$('<h5>').addClass('card-title').html(data.badge.badgeName)
	    			    				).append(
	   										$('<p>').addClass('card-text').html('FORM ' + data.badge.badgeForm)
	    			    				)
				    				)
			    				)
			    			);
			    		}
			    	} else {
						if (data.badge.badgeCategory === 'SENIOR BADGE') {
			    			$('#accordion-bodyOne').append(
			    				$('<div>').addClass('col-lg-3').addClass('col-12').addClass('my-1').append(
		    						$('<div>').addClass('card border border-black border-4 h-100').css('min-height', '360px')
			    				)
			    			);
			    		} else if (data.badge.badgeCategory === 'JUNIOR BADGE') {
			    			$('#accordion-bodyTwo').append(
			    				$('<div>').addClass('col-lg-3').addClass('col-12').addClass('my-1').append(
		    						$('<div>').addClass('card border border-black border-4 h-100').css('min-height', '360px')
			    				)
			    			);
			    		}
			    	}
		    	}
		    });
		}
		
		function onGetAssessmentApplication() {
			$.ajax({
				type: "GET",
		        url: "/core/student/assessments/applications/" + sessionStorage.getItem('login_id')
		    }).then(function(dataList) {
		    	let existStatus = false;
		    	for(let data of dataList) {
		    		if (data.assessmentResponse.assessmentName === 'KING SCOUT' && data.studentAssessmentId && data.studentAssessmentResult == 'PASS') {
		    			existStatus = true;
		    			$('#accordian-zero').removeClass('d-none');
		    		}
		    	}
		    	
		    	if (!existStatus) {
		    		$('#accordian-zero').remove();
		    	}
		    });
		}
	</script>
<%@ include file="sidebar/sidebar_footer.jspf" %>