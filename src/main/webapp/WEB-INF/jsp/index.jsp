<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Form</title>
    <style>
        body {
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            background-color: #f1f5f9;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
            margin: 0;
        }

        .container {
            background-color: #ffffff;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            overflow: hidden;
            width: 350px;
        }

        .header {
            background-color: #1a73e8;
            color: #ffffff;
            text-align: center;
            padding: 20px 0;
        }

        .form {
            padding: 20px;
        }

        .form label {
            display: block;
            margin-bottom: 8px;
            color: #333333;
            font-size: 12px; /* Adjusted font size */
        }

        .form input {
            width: 100%;
            padding: 10px;
            margin-bottom: 16px;
            border: 1px solid #cccccc;
            border-radius: 4px;
            box-sizing: border-box;
            font-size: 12px;
        }

        .form button {
            width: 100%;
            padding: 12px;
            border: none;
            border-radius: 4px;
            background-color: #1a73e8;
            color: #ffffff;
            font-size: 12px;
            cursor: pointer;
            margin-top: 10px; /* Added margin-top for spacing */
        }

        .form button:hover {
            background-color: #155b9a;
        }

        .select-user-type {
            margin-bottom: 16px;
        }

        .select-user-type label {
            display: block;
            margin-bottom: 8px;
            color: #333333;
            font-size: 12px; /* Adjusted font size */
        }

        .select-user-type select {
            width: 100%;
            padding: 10px;
            border: 1px solid #cccccc;
            border-radius: 4px;
            box-sizing: border-box;
            font-size: 12px;
        }
        .toggle-password {
            display: flex;
            margin-top: 3px;
            font-size: 12px; /* Adjusted font size */
        }

        .toggle-password label {
            color: #333333;
        }


        .animation-container {
            text-align: right;
            max-width: 100%; /* Adjust max-width as needed */
            overflow: hidden;
            margin-bottom: 20px;  /* Adjust the margin as needed */
        }

        .list {
            display: inline-block;
            font-size: 12px;
            white-space: nowrap; /* Prevent text from wrapping */
            overflow: hidden;
            -webkit-animation: loop 20s infinite linear;
            animation: moveText linear 24s forwards infinite;
        }

        .item {
            color: #808080;
        }

        @keyframes moveText{
            from {
                transform: translateX(100%);
            }
            to {
                transform: translateX(-150%);
            }
        }
        .center-image {
            text-align: center;
        }

        .center-image img {
            width: 88%;
            display: inline-block; /* Ensure that margins work */
            margin: 0 auto; /* Center the image */
        }
        
    </style>
</head>
<body>
    <div class="container"><br>
        <div class="center-image">
            <img class="mt-4" alt="Logo" src="/image/TOP.png">
        </div>
        <div class="form">
            <div class="select-user-type">
                <label for="userType">Login As:</label>
                <select id="userType" onchange="updateLoginForm()">
                    <option value="TEACHER">Teacher</option>
                    <option value="EVALUATOR">Evaluator</option>
                    <option value="STUDENT">Student</option>
                </select>
            </div>

            <form id="loginForm" onsubmit="onLogin(event)" method="post">
                <label for="loginUsername">Email</label>
                <input type="text" name="loginUsername" placeholder="Enter Your Email" id="loginUsername" />

                <label for="loginPassword">Password</label>
                <input type="password" name="loginPassword" placeholder="Enter Your Password" id="loginPassword" />
                <div class="toggle-password">
                <label for="togglePassword" style="font-size: 11px;">Show_Password</label>
                	<input type="checkbox" id="togglePassword" onchange="togglePasswordVisibility()">
                </div>

                <div class="animation-container">
                    <div class="list">Teacher : Username =  Teacher email & Password = 2 Inital name + phone number</div>
                    <div class="list">Student : Username = Student email & Password = dlp + 12 digit IC number</div>
                    <div class="list">Evaluator : Username = Evaluator email & Password = registered email under school</div>
                </div>

                <button type="submit">Login</button>
            </form>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/form2js@1.0.0/src/form2js.min.js"></script>

    <script>
        function updateLoginForm() {
            const userTypeSelect = document.getElementById('userType');
            const selectedUserType = userTypeSelect.value;
            const formHeader = document.getElementById('formHeader');
            formHeader.textContent = selectedUserType;
        }

        function onLogin(event) {
            event.preventDefault();

            const userTypeSelect = document.getElementById('userType');
            const loginForm = form2js('loginForm', null, false);
            const loginJSON = JSON.stringify(loginForm);

            const formData = new FormData();
            formData.append("loginRequest", new Blob([loginJSON], { type: "application/json" }));

            let loginURL, redirectURL;

            const selectedUserType = userTypeSelect.value;
            if (selectedUserType === 'TEACHER') {
                loginURL = '/user/teacher/login';
                redirectURL = '/user/teacher/view';
            } else if (selectedUserType === 'EVALUATOR') {
                loginURL = '/user/evaluator/login';
                redirectURL = '/core/student/assessment/evaluation';
            } else if (selectedUserType === 'STUDENT') {
                loginURL = '/user/student/login';
                redirectURL = '/common/membership/view';
            }

            $.ajax({
                type: "POST",
                url: loginURL,
                processData: false,
                contentType: false,
                cache: false,
                data: formData
            }).then(function (data) {
                if (data.loginStatus) {
                    sessionStorage.setItem("login_id", data.loginId);
                    sessionStorage.setItem("login_type", selectedUserType);
                    window.location.href = redirectURL;
                } else {
                    alert(data.loginError);
                }
            });
        }
        function togglePasswordVisibility() {
            const passwordInput = document.getElementById('loginPassword');
            const togglePasswordCheckbox = document.getElementById('togglePassword');

            // If the checkbox is checked, show the password; otherwise, hide it
            passwordInput.type = togglePasswordCheckbox.checked ? 'text' : 'password';
        }
    </script>
    <script>
        function updateLoginForm() {
            const userTypeSelect = document.getElementById('userType');
            const selectedUserType = userTypeSelect.value;
            const formHeader = document.getElementById('formHeader');
            formHeader.textContent = selectedUserType;
        }
    </script>
</body>
</html>
