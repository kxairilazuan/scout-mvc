<%@ include file="sidebar/sidebar_header.jspf" %>
	<div id="loader_id" class="loading d-none"></div>
	<div class="content">
		<%@ include file="top_nav.jspf" %>
		<div class="p-2">
			<div class="card w-100 mb-2">
				<h4 class="card-header" style="font-family: 'YourDesiredFont', sans-serif;">SENIOR BADGE</h4>
				<ul class="list-group list-group-flush">
					<li class="list-group-item p-1 overflow-auto">
						<table id="tableIdOne" class="table table-striped compact" style="width:100%">
							<thead>
								<tr>
									<th>STUDENT</th>
							 	</tr>
							</thead>
							<tbody></tbody>
						</table>
					</li>
				</ul>
			</div>
			<div class="card w-100">
				<h4 class="card-header" style="font-family: 'YourDesiredFont', sans-serif;">JUNIOR BADGE</h4>
				<ul class="list-group list-group-flush">
					<li class="list-group-item p-1 overflow-auto">
						<table id="tableIdTwo" class="table table-striped compact" style="width:100%">
							<thead>
								<tr>
									<th>STUDENT</th>
							 	</tr>
							</thead>
							<tbody></tbody>
						</table>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		function onWindowLoad() {
			$('#loader_id').removeClass('d-none');
			$.when(onGetStudentBadges(), onGetBadgesColumn()).done(function(a1, a2) {
				for(let data of a1[0]) {
					if (data.badgeCategory === 'SENIOR BADGE') {
						$('#tableIdOne > thead > tr:first').append($('<th>').append(data.badgeName));
					} else if (data.badgeCategory === 'JUNIOR BADGE') {
						$('#tableIdTwo > thead > tr:first').append($('<th>').append(data.badgeName));
					}
				}
		    	
				for(let data of a2[0]) {
					let tr = $('<tr>');
					tr.append($('<td>').append(data.studentResponse.studentName));
					for(let studentBadge of data.studentBadgeResponses) {
						if (studentBadge.badge.badgeCategory === 'SENIOR BADGE') {
							switch(studentBadge.studentBadgeStatus) {
								case 'PENDING APPROVAL':
									tr.append(
										$('<td>').addClass('p-2').append(
											$('<span>').addClass('py-1').addClass('px-2').addClass('fw-bold').addClass('text-warning').addClass('bg-warning-subtle').addClass('border').addClass('border-warning-subtle').addClass('rounded-2').append(studentBadge.studentBadgeStatus)
										)
									);
								break;
								case 'APPROVE':
									tr.append(
										$('<td>').addClass('p-2').append(
											$('<span>').addClass('py-1').addClass('px-2').addClass('fw-bold').addClass('text-success').addClass('bg-success-subtle').addClass('border').addClass('border-success-subtle').addClass('rounded-2').append(studentBadge.studentBadgeStatus)
										)
									);
								break;
								case 'NOT APPROVE':
									tr.append(
										$('<td>').addClass('p-2').append(
											$('<span>').addClass('py-1').addClass('px-2').addClass('fw-bold').addClass('text-danger').addClass('bg-danger-subtle').addClass('border').addClass('border-danger-subtle').addClass('rounded-2').append(studentBadge.studentBadgeStatus)
										)
									);
								break;
								default:
									tr.append(
										$('<td>').addClass('p-2').append('')
									);
							}
						}
					}
					$('#tableIdOne > tbody:last').append(tr);
				}
		    	
				for(let data of a2[0]) {
					let tr = $('<tr>');
					tr.append($('<td>').append(data.studentResponse.studentName));
					for(let studentBadge of data.studentBadgeResponses) {
						if (studentBadge.badge.badgeCategory === 'JUNIOR BADGE') {
							switch(studentBadge.studentBadgeStatus) {
								case 'PENDING APPROVAL':
									tr.append(
										$('<td>').addClass('p-2').append(
											$('<span>').addClass('py-1').addClass('px-2').addClass('fw-bold').addClass('text-warning').addClass('bg-warning-subtle').addClass('border').addClass('border-warning-subtle').addClass('rounded-2').append(studentBadge.studentBadgeStatus)
										)
									);
								break;
								case 'APPROVE':
									tr.append(
										$('<td>').addClass('p-2').append(
											$('<span>').addClass('py-1').addClass('px-2').addClass('fw-bold').addClass('text-success').addClass('bg-success-subtle').addClass('border').addClass('border-success-subtle').addClass('rounded-2').append(studentBadge.studentBadgeStatus)
										)
									);
								break;
								case 'NOT APPROVE':
									tr.append(
										$('<td>').addClass('p-2').append(
											$('<span>').addClass('py-1').addClass('px-2').addClass('fw-bold').addClass('text-danger').addClass('bg-danger-subtle').addClass('border').addClass('border-danger-subtle').addClass('rounded-2').append(studentBadge.studentBadgeStatus)
										)
									);
								break;
								default:
									tr.append(
										$('<td>').addClass('p-2').append('')
									);
							}
						}
					}
					$('#tableIdTwo > tbody:last').append(tr);
				}
				
				$('#tableIdOne').dataTable({
					"autoWidth": true,
					"info": false,
					"lengthChange": (window.innerWidth > 499) ? true : false,
					"pagingType": "simple_numbers",
					"bDestroy": true,
					"language": {
					    "decimal":        "",
					    "emptyTable":     "No data available in table",
					    "info":           "Showing _START_ to _END_ of _TOTAL_ entries",
					    "infoEmpty":      "Showing 0 to 0 of 0 entries",
					    "infoFiltered":   "(filtered from _MAX_ total entries)",
					    "infoPostFix":    "",
					    "thousands":      ",",
					    "lengthMenu":     "_MENU_",
					    "loadingRecords": "Loading...",
					    "processing":     "",
					    "search":         "",
					    "zeroRecords":    "No matching records found",
					    "paginate": {
					        "first":      "First",
					        "last":       "Last",
					        "next":       "&#11208;",
					        "previous":   "&#11207;"
					    },
					    "aria": {
					        "sortAscending":  ": activate to sort column ascending",
					        "sortDescending": ": activate to sort column descending"
					    }
					}
				});
		    	
				$('#tableIdTwo').dataTable({
					"autoWidth": true,
					"info": false,
					"lengthChange": (window.innerWidth > 499) ? true : false,
					"pagingType": "simple_numbers",
					"bDestroy": true,
					"language": {
					    "decimal":        "",
					    "emptyTable":     "No data available in table",
					    "info":           "Showing _START_ to _END_ of _TOTAL_ entries",
					    "infoEmpty":      "Showing 0 to 0 of 0 entries",
					    "infoFiltered":   "(filtered from _MAX_ total entries)",
					    "infoPostFix":    "",
					    "thousands":      ",",
					    "lengthMenu":     "_MENU_",
					    "loadingRecords": "Loading...",
					    "processing":     "",
					    "search":         "",
					    "zeroRecords":    "No matching records found",
					    "paginate": {
					        "first":      "First",
					        "last":       "Last",
					        "next":       "&#11208;",
					        "previous":   "&#11207;"
					    },
					    "aria": {
					        "sortAscending":  ": activate to sort column ascending",
					        "sortDescending": ": activate to sort column descending"
					    }
					}
				});
				$('#loader_id').addClass('d-none');
			});
		}

		function onGetBadgesColumn() {
			return $.ajax({
				type: "GET",
		        url: "/core/students/badges/progress"
		    })
		}
		
		function onGetStudentBadges() {
			return $.ajax({
				type: "GET",
		        url: "/core/badges"
		    })
		}
	</script>
<%@ include file="sidebar/sidebar_footer.jspf" %>