<%@ include file="sidebar/sidebar_header.jspf" %>
	<div id="loader_id" class="loading d-none"></div>
	<div class="content">
		<%@ include file="top_nav.jspf" %>
		<div class="p-2">
			<img class="d-none" style="width: 10%;" alt="School Logo" id="logoId" src="/image/JATA.png">
			<canvas width="100" height="100" class="d-none" id="canvasId"></canvas>
			<div class="card w-100">
				<h4 class="card-header">Application</h4>
				<ul class="list-group list-group-flush">
					<li class="list-group-item p-1 overflow-auto">
						<table id="assessmentTable" class="table table-striped compact" style="width:100%">
							<thead>
								<tr>
									<th>Assessment</th>
									<th>Status</th>
									<th>Evaluator</th>
									<th>Result</th>
									<th>Letter</th>
									<th>Date</th>
									<th>Action</th>
							 	</tr>
							</thead>
							<tbody></tbody>
						</table>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="modal fade" id="insertModal" data-bs-backdrop="static" data-bs-keyboard="false">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="insertForm" novalidate>
					<div class="modal-body">
						<div class="m-4 w-100 text-center">
							<img class="mt-4" style="width: 20%;" alt="Logo" src="/image/SCOUT.png">
							<h4 class="fw-bold">SCOUT ASSOCIATION OF MALAYSIA</h4>
							<h6>APPLICATION FORM<br><span id="name_label_id"></span></h6>
						</div>
						<div class="row g-1 pb-2">
							<div class="col-12">
								<input class="d-none" type="text" id="studentAssessmentId" name="studentAssessmentId" />
								<input class="d-none" type="text" id="studentId" name="studentId" />
								<input class="d-none" type="text" id="assessmentId" name="assessmentId" />
    							<label for="studentName" class="form-label">Full name</label>
    							<input type="text" class="form-control form-control-sm" id="studentName" disabled />
							</div>
						</div>
						<div class="row g-1 pb-2">
							<div class="col-lg-4 col-12">
    							<label for="studentIdentificationNo" class="form-label">IC Number</label>
    							<input type="text" class="form-control form-control-sm" id="studentIdentificationNo" disabled />
						     </div>
							<div class="col-lg-4 col-12">
    							<label for="studentBirthDate" class="form-label">Birth Date</label>
    							<input type="date" class="form-control form-control-sm" id="studentBirthDate" disabled />
							</div>
							<div class="col-lg-4 col-12">
    							<label for="studentAge" class="form-label">Age</label>
    							<input type="number" class="form-control form-control-sm" id="studentAge" disabled />
							</div>
						</div>
						<div class="row g-1 pb-2">
							<div class="col-12">
								<label for="studentAddress" class="form-label">Address</label>
								<textarea class="form-control" rows="3" id="studentAddress" disabled></textarea>
							</div>
						</div>
						<table id="normalTable">
							<tr>
								<th>BADGE</th>
								<th>SERIAL NUMBER</th>
								<th>APPROVAL DATE</th>
							</tr>
							<tbody id="badgeTable"></tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" onclick="resetForm('insertForm')" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Close</button>
						<button id="apply_btn_id" type="submit" class="btn btn-sm btn-success">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade" id="notEligibleModal">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h1 class="modal-title fs-5" id="exampleModalLabel">Attention</h1>
	                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	            </div>
	            <div class="modal-body">You're not eligible to apply for the evaluation because you did not meet the requirements necessary.</div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Close</button>
	            </div>
	        </div>
	    </div>
	</div>
	
	<script type="text/javascript">
		function onWindowLoad() {
			onGetTableData();
		}
		
		function onGetTableData() {
			$.ajax({
				type: "GET",
		        url: "/core/student/assessments/applications/" + sessionStorage.getItem('login_id')
		    }).then(function(dataList) {
		    	for(let data of dataList) {
		    		let formattedDate = '';
		    		if (data.studentAssessmentResultDate) {
		    			let date = new Date(data.studentAssessmentResultDate);
		    			formattedDate = date.toLocaleDateString("en-GB");
		    		}
		    		
		    		let tr = $('<tr>');
		    		let td1 = $('<td>').append(data.assessmentResponse.assessmentName);
		    		let td2 = $('<td>');
		    		if (data.assessmentId) {
		    			td2.append($('<span>').addClass('py-1').addClass('px-2').addClass('fw-bold').addClass('text-success').addClass('bg-success-subtle').addClass('border').addClass('border-success-subtle').addClass('rounded-2').append('APPLIED'));
		    		} else {
		    			td2.append($('<span>').addClass('py-1').addClass('px-2').addClass('fw-bold').addClass('text-warning').addClass('bg-warning-subtle').addClass('border').addClass('border-warning-subtle').addClass('rounded-2').append('NOT APPLIED'));
		    		}
		    		let td3 = $('<td>').append(data.evaluatorResponse ? data.evaluatorResponse.evaluatorName : '');
		    		
		    		let td4 = $('<td>');
		    		if (data.studentAssessmentResult) {
		    			if (data.studentAssessmentResult === 'PASS') {
		    				td4.append($('<span>').addClass('py-1').addClass('px-2').addClass('fw-bold').addClass('text-success').addClass('bg-success-subtle').addClass('border').addClass('border-success-subtle').addClass('rounded-2').append(data.studentAssessmentResult));
		    			} else if (data.studentAssessmentResult === 'FAIL') {
		    				td4.append($('<span>').addClass('py-1').addClass('px-2').addClass('fw-bold').addClass('text-danger').addClass('bg-danger-subtle').addClass('border').addClass('border-danger-subtle').addClass('rounded-2').append(data.studentAssessmentResult));
		    			}
		    		} else {
		    			td4.append('');
		    		}
		    		let td5 = $('<td>');
		    		if (data.studentAssessmentResult) {
		    			if (data.studentAssessmentResult === 'PASS') {
		    				td5.append($('<button>').addClass('btn').addClass('btn-primary').addClass('btn-sm').append('LETTER').click(onGenerateLetter));
		    			} else if (data.studentAssessmentResult === 'FAIL') {
		    				td5.append('');
		    			}
		    		} else {
		    			td5.append('');
		    		}
		    		let td6 = $('<td>').append(formattedDate);
		    		let td7 = $('<td>').append('<button class="btn btn-sm btn-warning m-1" onclick="onCheckEligible(\''+data.assessmentResponse.assessmentId+'\')">Apply</button>');
		    		tr.append(td1);
		    		tr.append(td2);
		    		tr.append(td3);
		    		tr.append(td4);
		    		tr.append(td5);
		    		tr.append(td6);
		    		tr.append(td7);
		    		$('#assessmentTable > tbody:last').append(tr);
				}
		    	
				$('#assessmentTable').dataTable({
					"autoWidth": true,
					"info": false,
					"lengthChange": (window.innerWidth > 499) ? true : false,
					"pagingType": "simple_numbers",
					"bDestroy": true,
					"language": {
					    "decimal":        "",
					    "emptyTable":     "No data available in table",
					    "info":           "Showing _START_ to _END_ of _TOTAL_ entries",
					    "infoEmpty":      "Showing 0 to 0 of 0 entries",
					    "infoFiltered":   "(filtered from _MAX_ total entries)",
					    "infoPostFix":    "",
					    "thousands":      ",",
					    "lengthMenu":     "_MENU_",
					    "loadingRecords": "Loading...",
					    "processing":     "",
					    "search":         "",
					    "zeroRecords":    "No matching records found",
					    "paginate": {
					        "first":      "First",
					        "last":       "Last",
					        "next":       "&#11208;",
					        "previous":   "&#11207;"
					    },
					    "aria": {
					        "sortAscending":  ": activate to sort column ascending",
					        "sortDescending": ": activate to sort column descending"
					    }
					}
				});
		    });
		}
		
		$('#insertForm').submit(function(event){
			event.preventDefault();
			let formValidityStatus = checkFormValidity("insertForm");
			
			if (formValidityStatus) {
				let insertForm = form2js("insertForm", null, false);
				let insertJSON = JSON.stringify(insertForm);
				
				let formData = new FormData();
				formData.append("studentAssessmentRequest", new Blob([insertJSON], { type : "application/json" }));
				
				$.ajax({
					type: ($('#studentAssessmentId').val()) ? 'PUT' : 'POST',
			        url: "/core/studentAssessment",
			        processData: false,
			        contentType: false,
			        cache: false,
			        data: formData
			    }).then(function(data) {
			    	$('#insertModal').modal('hide');
			    	$('#assessmentTable').DataTable().clear().destroy();
			    	resetForm("insertForm");
			    	onGetTableData();
			    });
			}
		});
		
		function onCheckEligible(assessmentId) {
			$('#loader_id').removeClass('d-none');
			$.when(onGetBadgeRequirement(assessmentId), onGetStudentBadge(sessionStorage.getItem('login_id'))).done(function(a1, a2) {
				let studentBadgeList = a2[0].filter(i => i.studentBadgeStatus === 'APPROVE');
				let badgeList = a1[0].filter(badge => !studentBadgeList.some(studentBadge => studentBadge.badgeId === badge.badgeId));
				
				if (badgeList.length === 0) {
					onOpenModal(assessmentId);
				} else {
					$('#loader_id').addClass('d-none');
					$('#notEligibleModal').modal('show');
				}
			});
		}
		
		function onOpenModal(assessmentId) {
			let studentId = sessionStorage.getItem('login_id');
			
			$.when(onGetStudent(studentId), onGetAssessment(assessmentId), onGetStudentAssessment(studentId, assessmentId), onGetStudentBadge(studentId), onGetBadgeRequirement(assessmentId)).done(function(a1, a2, a3, a4, a5) {
			    $('#insertModal').modal('show');
				if (a3[0]) {
					$('#apply_btn_id').addClass('d-none');
			    	setForm('insertForm', a3[0]);
		    	} else {
					$('#apply_btn_id').removeClass('d-none');
		    		$('#studentId').val(studentId);
		    		$('#assessmentId').val(assessmentId);
		    	}
		    	
		    	if (a1[0]) {
		    		$('#studentName').val(a1[0].studentName);
		    		$('#studentIdentificationNo').val(a1[0].studentIdentificationNo);
		    		$('#studentAge').val(a1[0].studentAge);
		    		$('#studentAddress').val(a1[0].studentAddress);
		    		
		    		let jsDateTime = new Date(a1[0].studentBirthDate);
		    		let jsDateTimeOffset = new Date(jsDateTime.setMinutes(jsDateTime.getMinutes() - jsDateTime.getTimezoneOffset()));
		    		let birthDate = jsDateTimeOffset.toISOString().split('T')[0];
		    		$('#studentBirthDate').val(birthDate);
		    	}
		    	
		    	if (a2[0]) {
		    		$('#name_label_id').html(a2[0].assessmentName);
		    	}
		    	
		    	if (a4[0] && a5[0]) {
		    		$('#badgeTable').empty();
					let badgeList = a4[0].filter(studentBadge => a5[0].some(badge => badge.badgeId === studentBadge.badgeId));
		    		
		    		for(let studentBadge of badgeList) {
			    		let formattedDate = '';
			    		if (studentBadge.studentApprovalDate) {
			    			let date = new Date(studentBadge.studentApprovalDate);
			    			formattedDate = date.toLocaleDateString("en-GB");
			    		}
			    		
			    		if (studentBadge.studentBadgeStatus === 'APPROVE') {
			    			$('#normalTable > tbody:last').append($('<tr>')
								.append($('<td>').append(studentBadge.badge.badgeName))
								.append($('<td>').append(studentBadge.studentBadgeSerialNum))
								.append($('<td>').append(formattedDate))
							);
			    		}
		    		}
		    	}
				$('#loader_id').addClass('d-none');
			});
		}
		
		function onGetStudent(studentId) {
			return $.ajax({
				type: "GET",
		        url: "/user/student/" + studentId
		    });
		}
		
		function onGetAssessment(assessmentId) {
			return $.ajax({
				type: "GET",
		        url: "/core/assessment/" + assessmentId
		    });
		}
		
		function onGetStudentAssessment(studentId, assessmentId) {
			return $.ajax({
				type: "GET",
		        url: "/core/student/assessment/application/" + studentId + "/" + assessmentId
		    })
		}
		
		function onGetStudentBadge(studentId) {
			return $.ajax({
				type: "GET",
		        url: "/core/studentBadges/student/" + studentId
		    });
		}
		
		function onGetBadgeRequirement(assessmentId) {
			return $.ajax({
				type: "GET",
		        url: "/core/assessments/requirements/" + assessmentId
		    });
		}
		
		function onGenerateLetter() {
			$.ajax({
				type: "GET",
		        url: "/common/event"
		    }).then(function(data) {
		    	let formattedDate = '';
		    	if (data.eventDate) {
	    			let date = new Date(data.eventDate);
	    			formattedDate = date.toLocaleDateString("en-GB");
		    	}
		    	
		    	let docDefinition = {
	    			pageSize: 'A4',
	    			pageOrientation: 'potrait',
	    			pageMargins: [40, 40, 40, 40],
	    			content: [{
	    					table: {
	    						widths: [80, 250, 30, 200],
	    						body: [
	    							[{
	    								image: imageToDataURL(document.getElementById("canvasId")), alignment: 'center',
	    								fontSize: 8,
	    								rowSpan: 2
	    							}, {
	    								text: 'KEMENTERIAN PENDIDIKAN MALAYSIA',
	    								fontSize: 10,
	    								bold: true
	    							}, {
	    								text: '',
	    								fontSize: 8
	    							}, {
	    								text: '',
	    								fontSize: 8
	    							}],
	    							[{
	    								text: '',
	    								fontSize: 8
	    							}, {
	    								text: 'Pejabat Pendidikan Daerah Kulim Bandar Baharu \n Jabatan Pendidikan Negeri Kedah \n Jalan Sultan Badlishah \n 09000 Kulim, Kedah Darul Aman',
	    								fontSize: 10,
	    								lineHeight: 1.1
	    							}, {
	    								text: 'Tel \n Faks \n Portal \n E-mel',
	    								fontSize: 8,
	    								lineHeight: 1.2
	    							}, {
	    								text: ': 04-4907662 \n : 04-4913387 \n : ppdkbb.moe.gov.my \n : adminppdkbb@moe.gov.my',
	    								fontSize: 8,
	    								lineHeight: 1.2
	    							}],
	    						]
	    					},
	    					layout: 'noBorders'
	    				},
	    				{
	    					table: {
	    						headerRows: 0,
	    						widths: ['*'],
	    						body: [
	    							[{
	    								text: ''
	    							}],
	    							[{
	    								text: ''
	    							}]
	    						]
	    					},
	    					margin: [5, -30, 5, 5],
	    					layout: 'lightHorizontalLines'
	    				},
	    				{
	    					table: {
	    						widths: [300, 60, 150],
	    						body: [
	    							[{
	    								text: ''
	    							}, {
	    								text: 'Ruj. Kami \n Tarikh',
	    								fontSize: 8,
	    								lineHeight: 1.2
	    							}, {
	    								text: ': PPDKBB.100-6/4/4 (29) \n : 23/06/2019',
	    								fontSize: 8,
	    								lineHeight: 1.2
	    							}]
	    						]
	    					},
	    					layout: 'noBorders'
	    				},
	    				{
	    					text: 'Pengetua \n Sekolah-sekolah Daerah Kulim',
	    					style: 'normal',
	    					margin: [10, 60, 0, 10]
	    				},
	    				{
	    					text: 'Tuan,',
	    					style: 'normal',
	    					margin: [10, 10, 0, 10]
	    				},
	    				{
	    					text: 'PENYELARASAN AKTIVITI KOKURIKULUM: ISTIADAT PENANUGERAHAN SIJIL PENGAKAP RAJA TAHUN 2024',
	    					style: 'title',
	    					margin: [10, 10, 0, 5]
	    				},
	    				{
	    					text: 'Dengan segala hormatnya perkara tersebut di atas dirujuk. \n 1. Sukacita dimaklumkan, Persekutuan Pengakap Malaysia Negeri Kedah akan mengelolakan istiadat seperti berikut:',
	    					style: 'normal',
	    					margin: [10, 0, 0, 0]
	    				},
	    				{
	    					table: {
	    						widths: [50, 60, '*'],
	    						body: [
	    							[{
	    								text: ''
	    							}, {
	    								text: 'Tarikh \n Masa \n Tempat \n Pakaian',
	    								fontSize: 10,
	    								lineHeight: 1.3,
	    								bold: true
	    							}, {
	    								text: ': '+formattedDate+' \n : '+data.eventTime+' \n : '+data.eventVenue+' \n : '+data.eventDressCode,
	    								fontSize: 10,
	    								lineHeight: 1.3
	    							}]
	    						]
	    					},
	    					layout: 'noBorders',
	    					margin: [0, 10, 0, 10]
	    				},
	    				{
	    					text: '2. Sehubungan itu, tuan dan guru pengakap sekolah tuan dijemput hadir. Sila patuhi etika pakaian seperti di atas. Pegawai berurusan Encik Nizar Othman Persuruhanjaya Pengakap Daerah Kulim boleh dihubungi untuk keterangan lanjut.',
	    					style: 'normal',
	    					margin: [10, 0, 0, 0]
	    				},
	    				{
	    					text: 'Kerjasama dan sokongan tuan didahului dengan ucapan terima kasih.',
	    					style: 'normal',
	    					margin: [10, 10, 0, 10]
	    				},
	    				{
	    					text: '"BERKHIDMAT UNTUK NEGARA"',
	    					style: 'footer',
	    					margin: [10, 10, 0, 10]
	    				},
	    				{
	    					text: '"PENDIDIKAN CERMERLANG, KEDAH TERBILANG"',
	    					style: 'footer',
	    					margin: [10, 10, 0, 40]
	    				},
	    				{
	    					text: 'Saya yang menurut perintah,',
	    					style: 'normal',
	    					margin: [10, 0, 0, 0]
	    				},
	    				{
	    					text: 'Amin Faizol',
	    					style: 'signature',
	    					margin: [10, 0, 0, 0]
	    				},
	    				{
	    					text: 'Pegawai Pelajaran Daerah Kulim.',
	    					style: 'footer',
	    					margin: [10, 0, 0, 0]
	    				},
	    				{
	    					table: {
	    						headerRows: 0,
	    						widths: ['*'],
	    						body: [
	    							[{
	    								text: ''
	    							}],
	    							[{
	    								text: ''
	    							}]
	    						]
	    					},
	    					margin: [0, 100, 0, 0],
	    					layout: 'lightHorizontalLines'
	    				},
	    				{
	    					text: 'Sila catatkan rujukan jabatan ini apabila berhubung',
	    					fontSize: 6,
	    					italics: true,
	    					alignment: 'center'
	    				}
	    			],
	    			styles: {
	    				normal: {
	    					fontSize: 10,
	    					lineHeight: 1.3
	    				},
	    				title: {
	    					fontSize: 10,
	    					bold: true,
	    					decoration: 'underline'
	    				},
	    				footer: {
	    					fontSize: 10,
	    					bold: true
	    				},
	    				signature: {
	    					fontSize: 10,
	    					italics: true,
	    					lineHeight: 1.3
	    				}
	    			}
	    		};

	    		pdfMake.createPdf(docDefinition).open();
		    });
			
			function imageToDataURL(canvas) {
				let context = canvas.getContext("2d");
				let image = document.getElementById("logoId");
				context.drawImage(image, 0, 0, 80, 60);
				return canvas.toDataURL();
			}
		}
	</script>
<%@ include file="sidebar/sidebar_footer.jspf" %>