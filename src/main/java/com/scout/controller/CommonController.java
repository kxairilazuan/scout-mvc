package com.scout.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.scout.dto.MembershipRequest;
import com.scout.dto.MembershipResponse;
import com.scout.dto.StudentResponse;
import com.scout.dto.EventResponse;
import com.scout.dto.GuardianRequest;
import com.scout.dto.GuardianResponse;
import com.scout.service.CommonService;
import com.scout.service.FilesStorageService;
import com.scout.service.ScoutService;
import com.scout.service.UserService;
import com.scout.utility.BaseUtility;
import java.util.*;

@Controller
@RequestMapping("/common")
public class CommonController {

	@Autowired
	CommonService commonService;

	@Autowired
	UserService userService;

	@Autowired
	ScoutService scoutService;
	
	@Autowired
	FilesStorageService storageService;
	
	// VIEW all memberships page
	@GetMapping("/membership/view")
	public String viewMembershipPage() {
		return "memberships";
	}
	
	@GetMapping("/payment/view")
	public String viewPaymentPage() {
		return "payments";
	}
	
	@GetMapping("/guardian/view")
	public String viewGuardianPage() {
		return "guardians";
	}
	
	// VIEW single membership page
	@GetMapping("/membership/update/{membershipId}")
	public String updateMembershipPage() {
		return "membership";
	}
	
	@GetMapping("/payment/update/{paymentId}")
	public String updatePaymentPage() {
		return "payment";
	}

	@GetMapping("/guardian/update/{guardianId}")
	public String updateGuardianPage() {
		return "guardian";
	}
	
	// (R) GET all memberships data
	@GetMapping("/memberships")
	@ResponseBody
	public List<MembershipResponse> getMemberships() {
		return commonService.getMemberships();
	}
	
	@GetMapping("/guardians")
	@ResponseBody
	public List<GuardianResponse> getGuardians() {
		return commonService.getGuardians();
	}
	
	// (R) GET single membership data by primary key
	@GetMapping("/event")
	@ResponseBody
	public EventResponse getOpenEvent() {
		return commonService.getOpenEvent();
	}
	
	@GetMapping("/membership/{membershipId}")
	@ResponseBody
	public MembershipResponse getMembership(@PathVariable("membershipId") String membershipId) {
		return commonService.getMembership(membershipId);
	}
	
	@GetMapping("/files/{membershipId}")
	@ResponseBody
	public String getFile(@PathVariable String membershipId) {
		MembershipResponse membershipResponse = commonService.getMembership(membershipId);
		
		if (BaseUtility.isObjectNotNull(membershipResponse)) {
			return Base64.getEncoder().encodeToString(storageService.retrieveFile(membershipResponse.getMembershipPayReceipt()));
		} else {
			return "";
		}
	}
	
	@GetMapping("/guardian/{guardianId}")
	@ResponseBody
	public GuardianResponse getGuardian(@PathVariable("guardianId") String guardianId) {
		return commonService.getGuardian(guardianId);
	}
	
	@GetMapping("/guardian/student/{studentId}")
	@ResponseBody
	public GuardianResponse getGuardianByStudentId(@PathVariable("studentId") String studentId) {
		return commonService.getGuardianByStudentId(studentId);
	}
	
	// (C) CREATE membership data
	@PostMapping("/membership")
	@ResponseBody
	public MembershipResponse insertMembership(@RequestPart(value="membershipRequest") MembershipRequest membershipRequest, @RequestParam(name = "receipt", required = false) MultipartFile receipt) {
		if (receipt != null) {
			String message = "";
			try {
				storageService.insertFile(receipt);
				message = "Complete upload file : " + receipt.getOriginalFilename();
			} catch (Exception e) {
				message = "Fail upload file : " + receipt.getOriginalFilename() + ". Error : " + e.getMessage();
			}
			System.out.println(message);
			
			membershipRequest.setMembershipPayReceipt(receipt.getOriginalFilename());
		}
		
		return commonService.insertMembership(membershipRequest);
	}
	
	@PostMapping("/guardian")
	@ResponseBody
	public GuardianResponse insertGuardian(@RequestPart(value="guardianRequest") GuardianRequest guardianRequest) {
		return commonService.insertGuardian(guardianRequest);
	}
	
	// (U) UPDATE membership data
	@PutMapping("/membership")
	@ResponseBody
	public MembershipResponse updateMembership(@RequestPart(value="membershipRequest") MembershipRequest membershipRequest, @RequestParam(name = "receipt", required = false) MultipartFile receipt) {

		if (receipt != null) {
			String message = "";
			try {
				storageService.insertFile(receipt);
				message = "Complete upload file : " + receipt.getOriginalFilename();
			} catch (Exception e) {
				message = "Fail upload file : " + receipt.getOriginalFilename() + ". Error : " + e.getMessage();
			}
			System.out.println(message);
		
			membershipRequest.setMembershipPayReceipt(receipt.getOriginalFilename());
		}
		
		return commonService.updateMembership(membershipRequest);
	}
	
	@PutMapping("/membership/approval")
	@ResponseBody
	public MembershipResponse updateMembershipApproval(@RequestPart(value="membershipRequest") MembershipRequest membershipRequest, @RequestParam(name = "receipt", required = false) MultipartFile receipt) {
		MembershipResponse membershipResponse = commonService.updateMembershipApproval(membershipRequest);
		StudentResponse studentResponse = userService.getStudent(membershipResponse.getStudentId());
		System.out.println("Student name : " + studentResponse.getStudentName());
		if (membershipRequest.getMembershipPayStatus().equals("REJECT") && BaseUtility.isObjectNotNull(studentResponse)) {
			System.out.println("Phone number : " + studentResponse.getStudentPhoneNo());
			String endpoint = "/checkNumberStatus";
		    String endpoint2 = "/sendText";
		    String phoneNumber = studentResponse.getStudentPhoneNo();
		    String textmsg = "Your payment submission for membership registration has been rejected. Please make a new submission or provide the correct receipt. Visit the SMKDLP website at https://sams.kwserverlab.site for further assistance.";
		    
		    scoutService.sendWhatsappNotification(endpoint, phoneNumber, "");
		    scoutService.sendWhatsappNotification(endpoint2, phoneNumber, textmsg);
		}
	    
		return membershipResponse;
	}
	
	@PutMapping("/guardian")
	@ResponseBody
	public GuardianResponse updateGuardian(@RequestPart(value="guardianRequest") GuardianRequest guardianRequest) {
		return commonService.updateGuardian(guardianRequest);
	}
	
	// (D) DELETE membership data by primary key
	@DeleteMapping("/membership/{membershipId}")
	@ResponseBody
	public Boolean deleteMembership(@PathVariable("membershipId") String membershipId) {
		return commonService.deleteMembership(membershipId);
	}
	
	@DeleteMapping("/guardian/{guardianId}")
	@ResponseBody
	public Boolean deleteGuardian(@PathVariable("guardianId") String guardianId) {
		return commonService.deleteGuardian(guardianId);
	}
}
