package com.scout.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.scout.entity.EventEntity;

public interface EventRepository extends JpaRepository<EventEntity, Long> {
	
	EventEntity findByEventId(String eventId);
	
	EventEntity findByEventStatus(String eventStatus);
	
	Integer deleteByEventId(String eventId);
}
