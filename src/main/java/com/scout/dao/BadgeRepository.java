package com.scout.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.scout.entity.BadgeEntity;

@Repository
public interface BadgeRepository extends JpaRepository<BadgeEntity, Long> {
	
	List<BadgeEntity> findAllByOrderByBadgeOrder();
		
	BadgeEntity findByBadgeId(String badgeId);
				
	Integer deleteByBadgeId(String badgeId);
}
