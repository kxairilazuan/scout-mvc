package com.scout.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.scout.entity.RequirementEntity;

public interface RequirementRepository extends JpaRepository<RequirementEntity, Long> {

	RequirementEntity findByRequirementId(String requirementId);
	
	List<RequirementEntity> findByAssessmentId(String assessmentId);
}
