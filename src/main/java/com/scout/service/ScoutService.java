package com.scout.service;

public interface ScoutService {
	
	void sendWhatsappNotification(String endPoint, String phoneNumber, String textMessage);
}
