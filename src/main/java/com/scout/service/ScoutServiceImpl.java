package com.scout.service;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.scout.utility.BaseUtility;

@Service
public class ScoutServiceImpl implements ScoutService {

	private final String apiUrl = "https://ws-api.kwserverlab.site/api";
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	@Override
	public void sendWhatsappNotification(String endPoint, String phoneNumber, String textMessage) {
		String url = "";
		
		if (BaseUtility.isNotBlank(textMessage)) {
		    url = apiUrl + endPoint + "?phone=6" + phoneNumber + "@c.us&text=" + textMessage + "&session=default";
		} else {
			url = apiUrl + endPoint + "?phone=6" + phoneNumber + "@c.us&session=default";
		}
	    
	    HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic YWRtaW4xOmFkbWluMQ==");
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

		HttpEntity<String> entity = new HttpEntity<>("body", headers);

		System.out.println(restTemplate().exchange(url, HttpMethod.GET, entity, String.class));
	}
}