package com.scout.dto;

import java.util.List;

public class ProgressResponse {

	private StudentResponse studentResponse;
	
	private List<StudentBadgeResponse> studentBadgeResponses;

	public StudentResponse getStudentResponse() {
		return studentResponse;
	}

	public void setStudentResponse(StudentResponse studentResponse) {
		this.studentResponse = studentResponse;
	}

	public List<StudentBadgeResponse> getStudentBadgeResponses() {
		return studentBadgeResponses;
	}

	public void setStudentBadgeResponses(List<StudentBadgeResponse> studentBadgeResponses) {
		this.studentBadgeResponses = studentBadgeResponses;
	}
}
