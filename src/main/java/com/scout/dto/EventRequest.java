package com.scout.dto;

import java.util.Date;

import jakarta.persistence.Column;

public class EventRequest {
	
	private String eventId;
	
	private Date eventDate;
	
	private String eventTime;
	
	private String eventVenue;
	
	private String eventDressCode;

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public String getEventTime() {
		return eventTime;
	}

	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}

	public String getEventVenue() {
		return eventVenue;
	}

	public void setEventVenue(String eventVenue) {
		this.eventVenue = eventVenue;
	}

	public String getEventDressCode() {
		return eventDressCode;
	}

	public void setEventDressCode(String eventDressCode) {
		this.eventDressCode = eventDressCode;
	}


}
