package com.scout.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "system_event")
public class EventEntity {
		
	@Id
	@Column(name = "event_id")
	private String eventId;
	
	@Column(name = "event_date")
	private Date eventDate;
	
	@Column(name = "event_time")
	private String eventTime;
	
	@Column(name = "event_venue")
	private String eventVenue;
	
	@Column(name = "event_dress_code")
	private String eventDressCode;
	
	@Column(name = "event_status")
	private String eventStatus;
	
	public String getEventId() {
		return eventId;
	}
	
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	
	public Date getEventDate() {
		return eventDate;
	}
	
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	public String getEventTime() {
		return eventTime;
	}
	
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	
	public String getEventVenue() {
		return eventVenue;
	}
	
	public void setEventVenue(String eventVenue) {
		this.eventVenue = eventVenue;
	}
	
	public String getEventDressCode() {
		return eventDressCode;
	}
	
	public void setEventDressCode(String eventDressCode) {
		this.eventDressCode = eventDressCode;
	}

	public String getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}
}
